package org.himagroup.dailydose;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Handler;
import android.os.StrictMode;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

import com.shephertz.app42.paas.sdk.android.App42API;
import com.shephertz.app42.paas.sdk.android.storage.StorageService;

import org.himagroup.dailydose.adapters.CategoryListViewAdapter;
import org.himagroup.dailydose.database.StoreToLocal;
import org.himagroup.dailydose.datatypes.Category;
import org.himagroup.dailydose.datatypes.News;
import org.himagroup.dailydose.datatypes.RelatedNewsContent;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class DailyDoseMain extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener,
        NewsFragment.OnNewsListFragmentInteractionListener,
        BookmarksFragment.OnListFragmentInteractionListener,
        RelatedNewsFragment.OnRNListFragmentInteractionListener {
    public static final String DETAIL = "DETAIL";
    public static final String BOOKMARK = "BOOKMARK";
    public static final String SHARE = "SHARE";


    public static final String PREFS_NAME = "DD_PREFS";
    ImageView splashImageView;
    boolean splashloading;
    StorageService storageService;
    ArrayList<Category> categories = new ArrayList<Category>();
    NavigationView navigationView;
    Toolbar toolbar;
    ActionBarDrawerToggle mDrawerToggle;
    News selectedNewsItem = null;
    private DrawerLayout mDrawerLayout;
    private GridView mDrawerList;
    private NewsFragment newsFragment;
    private BookmarksFragment bookmarksFragment;
    private RelatedNewsFragment relatedNewsFragment;
    private DetailViewFragment detailsFragment;
    private FragmentManager.OnBackStackChangedListener backStackListener = new FragmentManager.OnBackStackChangedListener() {
        @Override
        public void onBackStackChanged() {
            setNavIcon();
        }
    };
    private Map<String, Category> categoryMap = new HashMap<String, Category>();

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        //App42API.initialize(this.getApplicationContext(), "5ac7ccfa08c7a2b1fe75570bc772a9ebfb24b05982b13486df0aaaf83251778d",
        //       "8eede4e0f9d53f2ff36de7391e1fda4531efbdfd9b59dc8f0f06b8f5b3c60d84");
        App42API.initialize(this.getApplicationContext(), "4f6a246adcdaa3a02f7cf86d8a3e6c999a37a747c10aa000f660a20a97b8966f",
                "af09a985a2e96bed00a3e3ad2f2c0322f8e3a493532bfa013deaf44eaf0ac61d");
        storageService = App42API.buildStorageService();
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        super.onCreate(savedInstanceState);
        splashImageView = new ImageView(this);
        splashImageView.setFitsSystemWindows(true);
        splashImageView.setImageResource(R.mipmap.launch_screen);
        splashImageView.setScaleType(ImageView.ScaleType.FIT_XY);
        setContentView(splashImageView);
        splashloading = true;
        Handler h = new Handler();
        h.postDelayed(new Runnable() {
            public void run() {
                splashloading = false;
                showMainActivity(savedInstanceState);
            }
        }, 3000);

    }

    protected void showMainActivity(Bundle savedInstanceState) {
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();

        StrictMode.setThreadPolicy(policy);
        setContentView(R.layout.activity_daily_dose_main);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitle("");
        toolbar.setLogo(R.drawable.logo_header);

        if (toolbar != null) {
            setSupportActionBar(toolbar);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        final DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerList = (GridView) findViewById(R.id.navigation_drawer_list);
        mDrawerList.setAdapter(new CategoryListViewAdapter(this, categories));
        mDrawerToggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close) {

            /**
             * Called when a drawer has settled in a completely open state.
             */
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
            }

            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);
            }
        };
        drawer.setDrawerListener(mDrawerToggle);
        mDrawerToggle.syncState();
        TextView bookMarksNav = (TextView) findViewById(R.id.bookmarks);
        bookMarksNav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showBookmarksFragment();
                drawer.closeDrawer(GravityCompat.START);
            }
        });
        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        getFragmentManager().addOnBackStackChangedListener(backStackListener);
        loadCategoriesFromAssets();
        showNewsFragment();
    }

    public void showBookmarksFragment() {
        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction ft = fragmentManager.beginTransaction();
        if (bookmarksFragment == null) {
            bookmarksFragment = new BookmarksFragment();
        }
        Fragment f = fragmentManager.findFragmentById(R.id.fragment_for_replacement);
        if (f == null) {
            ft.add(R.id.fragment_for_replacement, bookmarksFragment, "bookmarks").addToBackStack("bookmarks");
        } else {
            ft.replace(R.id.fragment_for_replacement, bookmarksFragment, "bookmarks").addToBackStack("bookmarks");
        }
        ft.commit();
    }

    public void showRelatedNewsFragment() {
        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction ft = fragmentManager.beginTransaction();
        if (relatedNewsFragment == null) {
            relatedNewsFragment = new RelatedNewsFragment();
        }
        Fragment f = fragmentManager.findFragmentById(R.id.fragment_for_replacement);
        if (f == null) {
            ft.add(R.id.fragment_for_replacement, relatedNewsFragment, "relatedNews").addToBackStack("relatedNews");
        } else {
            ft.replace(R.id.fragment_for_replacement, relatedNewsFragment, "relatedNews").addToBackStack("relatedNews");
        }
        ft.commit();
    }

    public void showNewsFragment() {
        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction ft = fragmentManager.beginTransaction();
        if (newsFragment == null) {
            newsFragment = new NewsFragment();
        }
        Fragment f = fragmentManager.findFragmentById(R.id.fragment_for_replacement);
        if (f == null) {
            ft.add(R.id.fragment_for_replacement, newsFragment, "news").addToBackStack("news");
        } else {
            ft.replace(R.id.fragment_for_replacement, newsFragment, "news").addToBackStack("news");
        }
        ft.commit();
    }

    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            FragmentManager fragmentManager = getFragmentManager();
            if (fragmentManager != null) {
                Log.e("Main Activity", "count of fragments   " + fragmentManager.getBackStackEntryCount());
                if (fragmentManager.getBackStackEntryCount() > 1) {
                    fragmentManager.popBackStack();
                } else if (fragmentManager.getBackStackEntryCount() == 1) {
                    if (!splashloading) {
                        finish();
                    }
                } else {
                    if (!splashloading) {
                        finish();
                    }
                }
            }
        }
    }

    protected void setNavIcon() {
        int backStackEntryCount = getFragmentManager().getBackStackEntryCount();
        mDrawerToggle.setDrawerIndicatorEnabled(backStackEntryCount == 1);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.daily_dose_main, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (mDrawerToggle.isDrawerIndicatorEnabled()) {
            return true;
        }
        switch (id) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


    @Override
    public void onNewsListFragmentInteraction(News item, String tag) {
        if (tag.equalsIgnoreCase(DETAIL)) {
            this.selectedNewsItem = item;
            RelatedNewsContent.clearAll();
            RelatedNewsContent.addAll(selectedNewsItem.getRns());
            showRelatedNewsFragment();
        }
        if (tag.equalsIgnoreCase(BOOKMARK)) {
            StoreToLocal stl = StoreToLocal.createStoreToLocal(getApplicationContext());
            this.selectedNewsItem = item;
            stl.insert(selectedNewsItem.getHeader(), selectedNewsItem.getUrl(), selectedNewsItem.getImageUrl());
        }
        if (tag.equalsIgnoreCase(SHARE)) {
            this.selectedNewsItem = item;
            Intent share = new Intent(android.content.Intent.ACTION_SEND);
            share.setType("text/plain");
            share.putExtra(Intent.EXTRA_SUBJECT, selectedNewsItem.getHeader());
            share.putExtra(Intent.EXTRA_TEXT, selectedNewsItem.getUrl());
            startActivity(Intent.createChooser(share, "Select Share route"));
        }
    }

    public void showDetailFragment() {
        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction ft = fragmentManager.beginTransaction();
        detailsFragment = null;
        detailsFragment = new DetailViewFragment();
        detailsFragment.setSelectedNews(selectedNewsItem);

        Fragment f = fragmentManager.findFragmentById(R.id.fragment_for_replacement);
        if (f == null) {
            ft.add(R.id.fragment_for_replacement, detailsFragment, "details").addToBackStack("details");
        } else {
            ft.replace(R.id.fragment_for_replacement, detailsFragment, "details").addToBackStack("details");
        }
        ft.commit();

    }

    public Toolbar getToolbar() {
        return toolbar;
    }

    public void setToolbar(Toolbar toolbar) {
        this.toolbar = toolbar;
    }

    @Override
    public void onListFragmentInteraction(News item) {
        this.selectedNewsItem = item;
        showDetailFragment();
    }

    private void loadCategoriesFromAssets() {
        String[] ids = getResources().getStringArray(R.array.category_ids);
        String[] names = getResources().getStringArray(R.array.category_names);
        TypedArray images = getResources().obtainTypedArray(R.array.category_images);
        ArrayList<Category> categories = new ArrayList<Category>();
        for (int i = 0; i < ids.length; i++) {
            Category c = new Category();
            c.setId(ids[i]);
            c.setName(names[i]);
            Bitmap bMap = BitmapFactory.decodeResource(getResources(), images.getResourceId(i, 0));
            c.setImage(bMap);
            c.setSelected(checkCategorySelected(c.getName()));
            categories.add(c);
        }
        this.categories = categories;
        ((CategoryListViewAdapter) mDrawerList.getAdapter()).clear();
        ((CategoryListViewAdapter) mDrawerList.getAdapter()).addAll(categories);
        ((CategoryListViewAdapter) mDrawerList.getAdapter()).notifyDataSetChanged();
    }

    public boolean checkCategorySelected(String catName) {
        SharedPreferences settings = getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        if (settings.contains(catName)) {
            return settings.getBoolean(catName, true);
        } else {
            SharedPreferences.Editor editor = settings.edit();
            editor.putBoolean(catName, true);
            editor.commit();
            return true;
        }
    }

    public void setCategoryVal(String catName, boolean val) {
        SharedPreferences settings = getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = settings.edit();
        editor.putBoolean(catName, val);
        editor.commit();
    }

    @Override
    public void onRNListFragmentInteraction(News item) {
        this.selectedNewsItem = item;
        showDetailFragment();
    }
}
