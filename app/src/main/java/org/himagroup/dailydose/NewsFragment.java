package org.himagroup.dailydose;

import android.app.Fragment;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.lsjwzh.widget.recyclerviewpager.RecyclerViewPager;

import org.himagroup.dailydose.adapters.MainCardsViewAdapter;
import org.himagroup.dailydose.asynctasks.AsyncTaskInterface;
import org.himagroup.dailydose.asynctasks.GetNewsAsync;
import org.himagroup.dailydose.datatypes.News;
import org.himagroup.dailydose.datatypes.NewsCollection;
import org.himagroup.dailydose.datatypes.StatusMessage;

import java.util.ArrayList;
import java.util.Iterator;

/**
 * A fragment representing a list of Items.
 * <p/>
 * interface.
 */
public class NewsFragment extends Fragment implements AsyncTaskInterface {

    private static final String ARG_COLUMN_COUNT = "column-count";
    ArrayList<News> newsList = new ArrayList<News>();
    DailyDoseMain dailyDoseMain;
    GetNewsAsync gna;
    private int mColumnCount = 1;
    private OnNewsListFragmentInteractionListener mListener;
    private RecyclerViewPager mRecyclerView;
    private MainCardsViewAdapter mAdapter;
    private LinearLayoutManager mLayoutManager;


    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public NewsFragment() {
    }

    // TODO: Customize parameter initialization
    @SuppressWarnings("unused")
    public static NewsFragment newInstance(int columnCount) {
        NewsFragment fragment = new NewsFragment();
        Bundle args = new Bundle();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.dailyDoseMain = (DailyDoseMain) getActivity();
        mListener = this.dailyDoseMain;

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.content_daily_dose_main, container, false);
        // Set the adapter
        if (view instanceof RecyclerViewPager) {
            mRecyclerView = (RecyclerViewPager) view;
            mRecyclerView.setHasFixedSize(false);

            // use a linear layout manager
            mLayoutManager = new LinearLayoutManager(getActivity().getApplicationContext(), LinearLayoutManager.VERTICAL, false);
            mRecyclerView.setTriggerOffset(0.15f);
            mRecyclerView.setFlingFactor(0.25f);

            mRecyclerView.setLayoutManager(mLayoutManager);
            // specify an adapter (see also next example)

            mAdapter = new MainCardsViewAdapter(mListener, newsList);
            mRecyclerView.setAdapter(mAdapter);
            mRecyclerView.setItemAnimator(new DefaultItemAnimator());

        }
        gna = new GetNewsAsync(dailyDoseMain.storageService, this);
        gna.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        return view;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnNewsListFragmentInteractionListener) {
            mListener = (OnNewsListFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                + " must implement OnListFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onDone(Object object) {
        if (object instanceof ArrayList) {
            ArrayList<NewsCollection> newsCollections = (ArrayList<NewsCollection>) object;
            newsList.clear();
            Iterator<NewsCollection> iterator = newsCollections.iterator();
            while (iterator.hasNext()) {
                NewsCollection n = iterator.next();
                News news = n.getNewses().get(0);
                news.setNoOfSources(n.getNewses().size());
                news.setCategory(n.getCategories()[0]);
                news.setRns(n.getNewses());
                newsList.add(news);
                Log.v("NEWS", news.toString());

            }
            mAdapter.swap(newsList);
        }
    }

    @Override
    public void onError(StatusMessage statusMessage) {

    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnNewsListFragmentInteractionListener {
        // TODO: Update argument type and name
        void onNewsListFragmentInteraction(News item, String tag);
    }
}
