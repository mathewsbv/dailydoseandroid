package org.himagroup.dailydose.adapters;

import android.os.AsyncTask;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import org.himagroup.dailydose.BookmarksFragment;
import org.himagroup.dailydose.R;
import org.himagroup.dailydose.asynctasks.BitmapDownloaderTask;
import org.himagroup.dailydose.datatypes.News;

import java.util.List;

/**
 */
public class BookmarksRecyclerViewAdapter extends RecyclerView.Adapter<BookmarksRecyclerViewAdapter.ViewHolder> {

    private final List<News> mValues;
    private final BookmarksFragment.OnListFragmentInteractionListener mListener;

    public BookmarksRecyclerViewAdapter(List<News> items, BookmarksFragment.OnListFragmentInteractionListener listener) {
        mValues = items;
        mListener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
            .inflate(R.layout.fragment_bookmarks, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mItem = mValues.get(position);
        if (holder.mImageView != null) {
            new BitmapDownloaderTask(holder.mImageView).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, mValues.get(position).getImageUrl());
        }
        holder.mContentView.setText(mValues.get(position).getHeader());

        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mListener) {
                    mListener.onListFragmentInteraction(holder.mItem);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final ImageView mImageView;
        public final TextView mContentView;
        public News mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            mImageView = (ImageView) view.findViewById(R.id.image);
            mContentView = (TextView) view.findViewById(R.id.content);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + mContentView.getText() + "'";
        }
    }
}
