package org.himagroup.dailydose.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import org.himagroup.dailydose.DailyDoseMain;
import org.himagroup.dailydose.R;
import org.himagroup.dailydose.datatypes.Category;

import java.util.List;

/**
 * Created by Blessan V Mathews on 2/5/2016.
 */
public class CategoryListViewAdapter extends ArrayAdapter<Category> {
    DailyDoseMain mActivity;

    public CategoryListViewAdapter(DailyDoseMain mActivity, List<Category> objects) {

        super(mActivity.getApplicationContext(), 0, objects);
        this.mActivity = mActivity;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final Category category = getItem(position);
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.category_list_item, parent, false);
        }
        TextView main = (TextView) convertView.findViewById(R.id.textView);
        main.setText(category.getName());
        ImageView im = (ImageView) convertView.findViewById(R.id.imageView);
        im.setImageBitmap(category.getImage());
        final ImageView im1 = (ImageView) convertView.findViewById(R.id.imageViewSelected);
        if (category.isSelected()) {
            im1.setVisibility(View.VISIBLE);
        } else {
            im1.setVisibility(View.GONE);
        }
        im.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (category.isSelected()) {
                    category.setSelected(false);
                    im1.setVisibility(View.GONE);
                    mActivity.setCategoryVal(category.getName(), false);

                } else {
                    category.setSelected(true);
                    im1.setVisibility(View.VISIBLE);
                    mActivity.setCategoryVal(category.getName(), true);
                }

            }


        });
        return convertView;
    }
}
