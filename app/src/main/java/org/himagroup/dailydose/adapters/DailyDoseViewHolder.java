package org.himagroup.dailydose.adapters;

import android.os.AsyncTask;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.lsjwzh.widget.recyclerviewpager.RecyclerViewPager;

import org.himagroup.dailydose.R;
import org.himagroup.dailydose.asynctasks.BitmapDownloaderTask;
import org.himagroup.dailydose.datatypes.News;

/**
 * Created by Blessan V Mathews on 2/5/2016.
 */
public class DailyDoseViewHolder extends RecyclerViewPager.ViewHolder {
    public View dailyDoseNewsView;
    public News associatedNews;
    public TextView title;
    public ImageView bookmarkItem;
    public ImageView shareItem;
    public TextView otherLinks;

    private boolean viewAttached = false;

    public DailyDoseViewHolder(View itemView) {
        super(itemView);
        dailyDoseNewsView = itemView;
    }

    public News getAssociatedNews() {
        return associatedNews;
    }

    public void setAssociatedNews(News associatedNews) {
        this.associatedNews = associatedNews;
        View currentView = dailyDoseNewsView;
        title = (TextView) currentView.findViewById(R.id.title);
        title.setText(associatedNews.getHeader());
        ImageView im = (ImageView) currentView.findViewById(R.id.newsImage);
        bookmarkItem = (ImageView) currentView.findViewById(R.id.bookmarkItem);
        shareItem = (ImageView) currentView.findViewById(R.id.shareItem);
        otherLinks = (TextView) currentView.findViewById(R.id.otherlinks);
        otherLinks.setText(associatedNews.getSourceName() + " +" + associatedNews.getNoOfSources() + " more");
        if (im != null) {
            new BitmapDownloaderTask(im).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, associatedNews.getImageUrl());
        }
    }

    public boolean isViewAttached() {
        return viewAttached;
    }

    public void setViewAttached(boolean viewAttached) {
        this.viewAttached = viewAttached;
        ImageView im = (ImageView) dailyDoseNewsView.findViewById(R.id.newsImage);
        LinearLayout toolsList = (LinearLayout) dailyDoseNewsView.findViewById(R.id.toolslist);
        if (this.viewAttached) {
            im.setVisibility(View.VISIBLE);
            toolsList.setVisibility(View.VISIBLE);
        } else {
            im.setVisibility(View.GONE);
            toolsList.setVisibility(View.GONE);
        }
    }
}
