package org.himagroup.dailydose.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.himagroup.dailydose.DailyDoseMain;
import org.himagroup.dailydose.NewsFragment;
import org.himagroup.dailydose.R;
import org.himagroup.dailydose.datatypes.News;

import java.util.ArrayList;

/**
 * Created by Blessan V Mathews on 2/5/2016.
 */
public class MainCardsViewAdapter extends RecyclerView.Adapter<DailyDoseViewHolder> {
    private final NewsFragment.OnNewsListFragmentInteractionListener mListener;
    private ArrayList<News> data;
    private int expandedPosition = 0;

    public MainCardsViewAdapter(NewsFragment.OnNewsListFragmentInteractionListener mListener, ArrayList<News> data) {

        this.mListener = mListener;
        this.data = data;
    }

    @Override
    public DailyDoseViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.daily_dose_cards_layout, parent, false);
        DailyDoseViewHolder vh = new DailyDoseViewHolder(v);
        return vh;
    }


    @Override
    public void onBindViewHolder(final DailyDoseViewHolder holder, int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        holder.setAssociatedNews(data.get(position));
        holder.dailyDoseNewsView.setId(position);

        holder.otherLinks.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mListener) {
                    mListener.onNewsListFragmentInteraction(holder.associatedNews, DailyDoseMain.DETAIL);
                }
            }
        });
        holder.bookmarkItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (null != mListener) {
                    mListener.onNewsListFragmentInteraction(holder.associatedNews, DailyDoseMain.BOOKMARK);
                }
            }
        });
        holder.shareItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (null != mListener) {
                    mListener.onNewsListFragmentInteraction(holder.associatedNews, DailyDoseMain.SHARE);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public ArrayList<News> getData() {

        return data;
    }

    public void setData(ArrayList<News> data) {
        this.data = data;
    }

    @Override
    public void onViewAttachedToWindow(DailyDoseViewHolder holder) {
        super.onViewAttachedToWindow(holder);
    }

    @Override
    public void onViewDetachedFromWindow(DailyDoseViewHolder holder) {
        super.onViewDetachedFromWindow(holder);
    }


    public void swap(ArrayList<News> newsList) {
        notifyDataSetChanged();
    }
}
