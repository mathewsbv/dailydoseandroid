package org.himagroup.dailydose.adapters;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;

import org.himagroup.dailydose.datatypes.Category;

import java.util.List;

/**
 * Created by Blessan V Mathews on 2/5/2016.
 */
public class NavigationListViewAdapter extends ArrayAdapter<Category> implements AdapterView.OnItemSelectedListener {
    public NavigationListViewAdapter(Context context, int resource, List<Category> objects) {
        super(context, resource, objects);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return super.getView(position, convertView, parent);
    }

    @Override
    public long getItemId(int position) {
        return super.getItemId(position);
    }

    @Override
    public Category getItem(int position) {
        return super.getItem(position);
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}
