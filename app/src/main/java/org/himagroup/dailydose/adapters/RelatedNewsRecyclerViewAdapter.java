package org.himagroup.dailydose.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.himagroup.dailydose.R;
import org.himagroup.dailydose.RelatedNewsFragment;
import org.himagroup.dailydose.datatypes.News;

import java.util.List;

/**
 * TODO: Replace the implementation with code for your data type.
 */
public class RelatedNewsRecyclerViewAdapter extends RecyclerView.Adapter<RelatedNewsRecyclerViewAdapter.ViewHolder> {

    private final List<News> mValues;
    private final RelatedNewsFragment.OnRNListFragmentInteractionListener mListener;

    public RelatedNewsRecyclerViewAdapter(List<News> items, RelatedNewsFragment.OnRNListFragmentInteractionListener listener) {
        mValues = items;
        mListener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
            .inflate(R.layout.fragment_related_news, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mItem = mValues.get(position);
        holder.mIdView.setText(mValues.get(position).getSourceName());
        holder.mContentView.setText(mValues.get(position).getHeader());

        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mListener) {
                    // Notify the active callbacks interface (the activity, if the
                    // fragment is attached to one) that an item has been selected.
                    mListener.onRNListFragmentInteraction(holder.mItem);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView mIdView;
        public final TextView mContentView;
        public News mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            mIdView = (TextView) view.findViewById(R.id.sourceName);
            mContentView = (TextView) view.findViewById(R.id.header);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + mContentView.getText() + "'";
        }
    }
}
