package org.himagroup.dailydose.asynctasks;


import org.himagroup.dailydose.datatypes.StatusMessage;

/**
 * Created by Blessan V Mathews on 23-05-2015.
 */
public interface AsyncTaskInterface {
    void onDone(Object object); // dummy data

    void onError(StatusMessage statusMessage);
}
