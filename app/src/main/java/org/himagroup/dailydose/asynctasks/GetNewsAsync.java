package org.himagroup.dailydose.asynctasks;

import android.os.AsyncTask;
import android.util.Log;

import com.shephertz.app42.paas.sdk.android.App42CallBack;
import com.shephertz.app42.paas.sdk.android.storage.Storage;
import com.shephertz.app42.paas.sdk.android.storage.StorageService;

import org.himagroup.dailydose.datatypes.News;
import org.himagroup.dailydose.datatypes.NewsCollection;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Blessan V Mathews on 2/11/2016.
 */
public class GetNewsAsync extends AsyncTask<Void, Void, ArrayList<NewsCollection>> {

    private ArrayList<NewsCollection> newses;
    private StorageService storageService;
    private boolean complete = false;
    private AsyncTaskInterface update;

    public GetNewsAsync(StorageService storageService, AsyncTaskInterface update) {
        this.storageService = storageService;
        this.update = update;
    }

    @Override
    protected ArrayList<NewsCollection> doInBackground(Void... params) {
        getCategories();
        while (!complete) {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        return this.newses;
    }

    private void getCategories() {
        String dbName = "DDDB";
        String collectionName = "News Feed";
        this.newses = new ArrayList<NewsCollection>();
        storageService.findAllDocuments(dbName, collectionName, new App42CallBack() {
            public void onSuccess(Object response) {
                Storage storage = (Storage) response;
                //This will return JSONObject list, however since Object Id is unique, list will only have one object
                ArrayList<Storage.JSONDocument> jsonDocList = storage.getJsonDocList();
                for (int i = 0; i < jsonDocList.size(); i++) {
                    try {

                        JSONObject jsonObject = new JSONObject(jsonDocList.get(i).getJsonDoc());
                        Log.v("STORAGE", jsonObject.toString());
                        newses.add(getNewsCollectionFromJSON(jsonObject));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                complete = true;
            }

            public void onException(Exception ex) {
                System.out.println("Exception Message" + ex.getMessage());
            }
        });
    }

    private NewsCollection getNewsCollectionFromJSON(JSONObject obj) throws JSONException {
        NewsCollection newsCollection = new NewsCollection();
        JSONArray array = obj.getJSONArray("news");
        ArrayList<News> newses = new ArrayList<News>();

        for (int i = 0; i < array.length(); i++) {
            newses.add(getNewsFromJSON(array.getJSONObject(i)));
        }
        newsCollection.setNewses(newses);
        if (obj.get("category_id") instanceof JSONArray) {
            newsCollection.setCategories(toStringArray(obj.getJSONArray("category_id")));
        } else if (obj.get("category_id") instanceof String) {
            String[] categories = new String[1];
            categories[0] = obj.getString("category_id");
            newsCollection.setCategories(categories);
        }
        return newsCollection;
    }

    private News getNewsFromJSON(JSONObject obj) {
        News c = new News();
        try {
            c.setUrl(obj.getString("url"));
        } catch (JSONException e) {
            c.setImageUrl("");
        }
        try {
            c.setImageUrl(obj.getString("imgurl"));
        } catch (JSONException e) {
            c.setImageUrl("");
        }
        try {
        c.setHeader(obj.getString("header"));
        } catch (JSONException e) {
            c.setImageUrl("");
        }
        return c;
    }
    @Override
    protected void onPostExecute(ArrayList<NewsCollection> newses) {
        super.onPostExecute(newses);
        update.onDone(newses);
    }

    public String[] toStringArray(JSONArray array) {
        if (array == null)
            return null;

        String[] arr = new String[array.length()];
        for (int i = 0; i < arr.length; i++) {
            arr[i] = array.optString(i);
        }
        return arr;
    }
}
