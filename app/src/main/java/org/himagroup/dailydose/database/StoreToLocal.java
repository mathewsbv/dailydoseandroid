package org.himagroup.dailydose.database;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import org.himagroup.dailydose.datatypes.News;

import java.util.ArrayList;

/**
 * Created by Blessan V Mathews on 2/11/2016.
 */
public class StoreToLocal extends SQLiteOpenHelper {
    private static final int DATABASE_VERSION = 1;
    private static final String DB_NAME = "dailydose.db";
    private static final String TABLE_NAME = "bookmarks";
    public static SQLiteDatabase db;

    /**
     * Constructor
     *
     * @param context the application context
     */
    private StoreToLocal(Context context) {
        super(context, DB_NAME, null, DATABASE_VERSION);
        db = getWritableDatabase();
    }

    public static StoreToLocal createStoreToLocal(Context context) {
        return new StoreToLocal(context);
    }

    /**
     * Called at the time to create the DB.
     * The create DB statement
     *
     * @param the SQLite DB
     */
    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(
                "create table " + TABLE_NAME + " (header text not null unique, imageUrl text, " +
                "url text ) ");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    /**
     * The Insert DB statement
     *
     * @param id   the friends id to insert
     * @param name the friend's name to insert
     */
    public void insert(String header, String url, String imageUrl) {
        db.execSQL("REPLACE  INTO " + TABLE_NAME + "('header', 'url', 'imageUrl' ) values ('"
            + header + "', '"
            + url + "', '"
            + imageUrl + "')");

    }

    public void deleteById(String id) {
        db.execSQL("DELETE FROM " + TABLE_NAME + "where 'id' = " + id);
    }

    /**
     * Select All that returns an ArrayList
     *
     * @return the ArrayList for the DB selection
     */
    public ArrayList<News> listSelectAll() {
        ArrayList<News> list = new ArrayList<News>();
        Cursor cursor = db.query(TABLE_NAME, new String[]{"header", "imageUrl", "url"},

            null, null, null, null, "header");
        if (cursor.moveToFirst()) {
            do {
                News f = new News();
                f.setHeader(cursor.getString(0));
                f.setImageUrl(cursor.getString(1));
                f.setUrl(cursor.getString(2));
                list.add(f);
            } while (cursor.moveToNext());
        }
        if (cursor != null && !cursor.isClosed()) {
            cursor.close();
        }
        return list;
    }

    /**
     * Wipe out the DB
     */
    public void clearAll() {
        db.delete(TABLE_NAME, null, null);
    }
}
