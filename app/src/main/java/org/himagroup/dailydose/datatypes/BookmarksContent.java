package org.himagroup.dailydose.datatypes;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Helper class for providing sample content for user interfaces created by
 * Android template wizards.
 * <p/>
 * TODO: Replace all uses of this class before publishing your app.
 */
public class BookmarksContent {

    /**
     * An array of sample (dummy) items.
     */
    public static final List<News> ITEMS = new ArrayList<News>();

    /**
     * A map of sample (dummy) items, by ID.
     */
    public static final Map<String, News> ITEM_MAP = new HashMap<String, News>();

    private static final int COUNT = 25;


    private static void addItem(News item) {
        ITEMS.add(item);
        // ITEM_MAP.put(item.getId(), item);
    }

    public static void addAll(ArrayList<News> items) {
        Iterator<News> iter = items.iterator();
        while (iter.hasNext()) {
            addItem(iter.next());
        }
    }


    private static String makeDetails(int position) {
        StringBuilder builder = new StringBuilder();
        builder.append("Details about Item: ").append(position);
        for (int i = 0; i < position; i++) {
            builder.append("\nMore details information here.");
        }
        return builder.toString();
    }


    public static void clearAll() {
        ITEMS.clear();
        ITEM_MAP.clear();
    }
}
