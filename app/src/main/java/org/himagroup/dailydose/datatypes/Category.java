package org.himagroup.dailydose.datatypes;

import android.graphics.Bitmap;

/**
 * Created by Blessan V Mathews on 2/5/2016.
 * <p>
 * <p>
 * <p>
 * category
 * <p>
 * {
 * "image_url": "someurl",
 * "name": "hollywood",
 * "id": "123"
 * }
 */
public class Category {
    private String imageUrl;
    private String name;
    private String id;
    private Bitmap image;
    private boolean selected = true;

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Bitmap getImage() {
        return image;
    }

    public void setImage(Bitmap image) {
        this.image = image;
    }
}
