package org.himagroup.dailydose.datatypes;

import com.google.common.net.InternetDomainName;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

/**
 * Created by Blessan V Mathews on 2/5/2016.
 */
public class News {
    private String header;
    private String imageUrl;
    private String url;
    private String sourceName;
    private int noOfSources;
    private String category;
    private ArrayList<News> rns = new ArrayList<News>();


    public String getHeader() {
        return header;
    }

    public void setHeader(String header) {
        this.header = header;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
        try {
            URL u = new URL(url);
            String host = u.getHost();
            this.sourceName = InternetDomainName.from(host).topPrivateDomain().parts().get(0);
        } catch (MalformedURLException me) {

        }
    }

    public String getSourceName() {
        return sourceName;
    }

    public void setSourceName(String sourceName) {
        this.sourceName = sourceName;
    }

    public int getNoOfSources() {
        return noOfSources;
    }

    public void setNoOfSources(int noOfSources) {
        this.noOfSources = noOfSources;
    }

    public ArrayList<News> getRns() {
        return rns;
    }

    public void setRns(ArrayList<News> rns) {
        this.rns = rns;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    @Override
    public String toString() {
        return "News{" +
                ", header='" + header + '\'' +
                ", imageUrl='" + imageUrl + '\'' +
                ", url='" + url + '\'' +
                ", sourceName='" + sourceName + '\'' +
                ", noOfSources='" + noOfSources + '\'' +
                '}';
    }
}
