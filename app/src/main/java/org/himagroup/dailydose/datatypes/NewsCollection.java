package org.himagroup.dailydose.datatypes;

import java.util.ArrayList;

/**
 * Created by MathewBV on 13-02-2016.
 */
public class NewsCollection {
    private ArrayList<News> newses;
    private String[] categories;

    public ArrayList<News> getNewses() {
        return newses;
    }

    public void setNewses(ArrayList<News> newses) {
        this.newses = newses;
    }

    public String[] getCategories() {
        return categories;
    }

    public void setCategories(String[] categories) {
        this.categories = categories;
    }
}
