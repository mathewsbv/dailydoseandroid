package org.himagroup.dailydose.datatypes;

/**
 * Created by Blessan V Mathews on 23-05-2015.
 */
public class StatusMessage {
    private String statusCode;
    private String error;
    private String message;

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}
